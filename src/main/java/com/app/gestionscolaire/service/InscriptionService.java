package com.app.gestionscolaire.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.gestionscolaire.entity.Inscription;
import com.app.gestionscolaire.repository.InscriptionRepository;

@Service
public class InscriptionService {
	
	@Autowired
	private InscriptionRepository repository;
	
	public Inscription SaveOneInscription(Inscription inscription) {
		return repository.save(inscription);
	}
	
	
	public List<Inscription> SaveManyInscription(List<Inscription> inscriptions){
		return repository.saveAll(inscriptions);
	}

	
	public Inscription GetInscriptionById(Long id) {
		return repository.findById(id).orElse(null);
	}
	

	public List<Inscription> getAllInscription(){
		return repository.findAll();
	}
	
	public Inscription UpdateInscription(Inscription inscription) {
		Inscription inscri = repository.findById(inscription.getIdInscription()).orElse(null);
		inscri.setDate(inscription.getDate());
		inscri.setEtat(inscription.getEtat());
		
		return repository.save(inscri);
	}
	
	public String DeleteInscription(Long id) {
		repository.deleteById(id);
		
		return "Inscription supprimer";
	}
	
}

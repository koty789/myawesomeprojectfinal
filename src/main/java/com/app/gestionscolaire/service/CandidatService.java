package com.app.gestionscolaire.service;

import com.app.gestionscolaire.entity.Candidat;
import com.app.gestionscolaire.entity.Users;
import com.app.gestionscolaire.repository.CandidatRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
public class CandidatService 
{
    @Autowired
    private CandidatRepository candidatRepo;

    
    public Candidat saveCandidat(Candidat candi)
    {
        return candidatRepo.save(candi);
    }
 
    public List<Candidat> getCandidats()
    {
    	return candidatRepo.findAll();
    	
    }
    
    public Candidat findCandidatById(Long id)
    {
    	return candidatRepo.getOne(id);
    }
    
}

package com.app.gestionscolaire.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.app.gestionscolaire.entity.Role;

import com.app.gestionscolaire.entity.Users;
import com.app.gestionscolaire.repository.UserRepository;


@Service
public class UserDetailsServiceImpl implements UserDetailsService
{
	@Autowired
	private UserRepository userRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Users user = userRepo.findByEmail(username);
		 if (user == null) 
		 {
	            throw new UsernameNotFoundException("User not found");
	     }
		 CustomUserDetails newUser=new CustomUserDetails(user);
	        return newUser ;
	    }
		
	}
	

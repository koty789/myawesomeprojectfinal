package com.app.gestionscolaire.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import com.app.gestionscolaire.entity.Role;
import com.app.gestionscolaire.entity.Users;

public class CustomUserDetails implements UserDetails {
    private static final long serialVersionUID = 1L;
    private Users user;

    public CustomUserDetails(Users user) {
        super();
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() 
    {
    	Set<Role> roles = user.getRoles();
    	List<SimpleGrantedAuthority> authorities=new ArrayList<>();
    	for(Role role:roles)
    	{
    		authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleName()));
    	}
		return authorities;
    }
    public String getPassword() {
        return this.user.getPassword();
    }
 
    @Override
    public String getUsername() {
        return this.user.getEmail();
    }
 
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
 
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
 
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
 
    @Override
    public boolean isEnabled() {
        return this.user.getActive();
    }
     
   
    

}
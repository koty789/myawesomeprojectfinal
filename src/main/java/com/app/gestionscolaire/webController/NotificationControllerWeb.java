package com.app.gestionscolaire.webController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.gestionscolaire.entity.Notification;
import com.app.gestionscolaire.service.NotificationService;

@Controller
public class NotificationControllerWeb {
	
	@Autowired
	private NotificationService service;

	@RequestMapping("/notification/list")
	public String ListDocument(Model model) {
		List<Notification> notifications = service.GetAllNotification();
		model.addAttribute("notifications", notifications);
		return "listNotifi";
	}
	
	
	@RequestMapping("/notification/new")
	public String NewNotification(Model model) {
		Notification notif = new Notification();
		model.addAttribute("notification", notif);
		return "newNotif";
	}
	
	
	@RequestMapping("notification/save")
	public String SaveInscrp(Model model, @ModelAttribute("notification") Notification notification) {
		service.SaveOneNotification(notification);
		return "redirect:/notification/list";
	}
	
	@GetMapping("notification/edit/{id}")
	public String EditerNotification(Model model, @PathVariable("id") Long id) {
		Notification notification = service.GetNotificationById(id);
		model.addAttribute("notification", notification);
		return "redirect:/notification/modifNotifi";
	}
	
	
	@PostMapping("notification/edition")
	public String EditNotification(@ModelAttribute("notification") Notification notification) {
		return "redirect:/notification/editNotification";
	}
	
	@RequestMapping("notification/delete")
	public String SupprimerNotification(Model model, @ModelAttribute("id") Long id) {
		service.DeleteNotification(id);
		return "redirect:/notification/ListNotification";
	}

}

package com.app.gestionscolaire.webController;


import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.app.gestionscolaire.entity.Admission;
import com.app.gestionscolaire.entity.Candidat;
import com.app.gestionscolaire.entity.Document;
import com.app.gestionscolaire.entity.Employe;
import com.app.gestionscolaire.entity.Inscription;
import com.app.gestionscolaire.entity.Role;
import com.app.gestionscolaire.entity.UploadFileResponse;
import com.app.gestionscolaire.entity.Users;
import com.app.gestionscolaire.repository.RoleRepository;
import com.app.gestionscolaire.service.AccountService;
import com.app.gestionscolaire.service.CandidatService;
import com.app.gestionscolaire.service.EmployeService;
import com.app.gestionscolaire.service.FileStorageService;
import com.app.gestionscolaire.service.UserDetailsServiceImpl;


@Controller
public class AdminControllerWeb

{
	///New modif
	@Autowired EmployeService empService;
	@Autowired
	private RoleRepository roleRepo;
	 @Autowired
	 private FileStorageService fileStorageService;
	 @Autowired
	 private PasswordEncoder bCryptPasswordEncoder;
	private org.springframework.security.core.Authentication authentication;
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;
	@Autowired
	private AccountService actservice;
	@Autowired
	private CandidatService candidatService;

	@GetMapping("/addCandidat")
	public String signin(ModelMap mod) 
	{   
		
		Users user=new Users();
		mod.addAttribute("user", user);		
		
		return "registrationCandidat";
	}
	
	@RequestMapping("addEmp")
	public String addEmp(ModelMap model)
	{
		Users user=new Users();
		List<Role> roles=actservice.findRoles();
		model.addAttribute("user", user);
		model.addAttribute("roles", roles);
		return "registrationEmploye";
	}
	
	@RequestMapping(value="/registrationEmp", method = RequestMethod.POST)
	public String addEmp( @RequestParam("userImage") MultipartFile multipartFile, ModelMap mod, @Valid Users user, BindingResult bindingResult,  HttpServletRequest request) throws IOException, MessagingException {
		
		
		String fileName= StringUtils.cleanPath(multipartFile.getOriginalFilename());
		// Lookup user in database by e-mail
		Users userExists = actservice.findByEmail(user.getEmail());
		
		System.out.println(userExists);
		
		if (userExists != null) {
			mod.addAttribute("userExistant", "Oups!  Il existe déjà un utilisateur avec cet adresse mail.");
			bindingResult.reject("email");
		}
			
		if (bindingResult.hasErrors()) 
		{ 
			return addEmp(mod);		
		} 
		else 
		{ // new user so we create user and send confirmation e-mail
			
			String passEncryp = bCryptPasswordEncoder.encode(user.getPassword());
			user.setPassword(passEncryp);
			//user.setRoles(new HashSet<>(roleRepo.findAll()));
			
			Role role=actservice.findRoleByRolename(user.getPosteName());
			Set<Role> roles=new HashSet<Role>();
			roles.add(role);
			user.setRoles(roles);
		    user.setActive(false);
		   
		    LocalDate today = LocalDate.now();
		    String user_vcode= today.toString();
		    
			user.setConfirmationToken(user_vcode);
						
		    Users userSave= actservice.saveUser(user);
		    //SetUserPhoto
			user.setPhoto(userSave.getIduser()+fileName);
			userSave= actservice.saveUser(user);
		    //Upload Image into directory
			String uploadDir= "./src/main/resources/static/images/users/";
		    Path uploadPath=Paths.get(uploadDir);
		    //If directory doesnt exist we will create it 
		    if(!Files.exists(uploadPath))
		    {
		    	Files.createDirectories(uploadPath);
		    }
		    
		    try(InputStream inputStream=multipartFile.getInputStream()) 
		    
		    {
		    	
				Path filePath = uploadPath.resolve(userSave.getIduser()+fileName);
				Files.copy(inputStream,filePath, StandardCopyOption.REPLACE_EXISTING);
		    	
		    	
			} 
		    catch (Exception e) 
		    
		    {
				throw new IOException("Le fichier suivant n'a pas pu être uploader" +fileName);
			}
		 
		    
		    String appUrl = request.getScheme() + "://" + request.getServerName()+":"+request.getServerPort();
		    
		    actservice.sendVerificationEmail(userSave, appUrl); 
		    mod.addAttribute("inscrip", "Inscription réussie! Un mail de validation vous a été envoyé sur votre adresse mail.");
		    return addEmp(mod);	
			
		}	 
		
	}	
	
	
	@RequestMapping("/notAutorized")
	public String error() {
		return "notAutorized";
	}
	
	@RequestMapping("/acceuil")
	public String acceuil() 
	{
		
		return "index1";
	}

	@GetMapping("/login")
	public String index(Model model,HttpServletRequest request) 
	{   
		
		return "login2";
	}	
	
	
	
	
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/login";
	}

	@RequestMapping(value="/adduser")
	public String addnewuser(ModelMap mod)
	{
		Users user=new Users();
		mod.addAttribute("user", user);		
		return "registrationUser";
	}	

	@RequestMapping(value="/registrationUser", method = RequestMethod.POST)
	public String adduser( @RequestParam("userImage") MultipartFile multipartFile, ModelMap mod, @Valid Users user, BindingResult bindingResult,  HttpServletRequest request) throws IOException, MessagingException {
		
		
		String fileName= StringUtils.cleanPath(multipartFile.getOriginalFilename());
		// Lookup user in database by e-mail
		Users userExists = actservice.findByEmail(user.getEmail());
		
		System.out.println(userExists);
		
		if (userExists != null) {
			mod.addAttribute("userExistant", "Oups!  Il existe déjà un utilisateur avec cet adresse mail.");
			bindingResult.reject("email");
		}
			
		if (bindingResult.hasErrors()) 
		{ 
			return signin(mod);		
		} 
		else 
		{ // new user so we create user and send confirmation e-mail
			
			String passEncryp = bCryptPasswordEncoder.encode(user.getPassword());
			user.setPassword(passEncryp);
			//user.setRoles(new HashSet<>(roleRepo.findAll()));
			
			Role role=actservice.findRoleByRolename("CANDIDAT");
			Set<Role> roles=new HashSet<Role>();
			roles.add(role);
			user.setRoles(roles);
		    user.setActive(false);
		   
		    LocalDate today = LocalDate.now();
		    String user_vcode= today.toString();
		    
			user.setConfirmationToken(user_vcode);
						
		    Users userSave= actservice.saveUser(user);
		    //SetUserPhoto
			user.setPhoto(userSave.getIduser()+fileName);
			userSave= actservice.saveUser(user);
		    //Upload Image into directory
			String uploadDir= "./src/main/resources/static/images/users/";
		    Path uploadPath=Paths.get(uploadDir);
		    //If directory doesnt exist we will create it 
		    if(!Files.exists(uploadPath))
		    {
		    	Files.createDirectories(uploadPath);
		    }
		    
		    try(InputStream inputStream=multipartFile.getInputStream()) 
		    
		    {
		    	
				Path filePath = uploadPath.resolve(userSave.getIduser()+fileName);
				Files.copy(inputStream,filePath, StandardCopyOption.REPLACE_EXISTING);
		    	
		    	
			} 
		    catch (Exception e) 
		    
		    {
				throw new IOException("Le fichier suivant n'a pas pu être uploader" +fileName);
			}
		 
		    
		    String appUrl = request.getScheme() + "://" + request.getServerName()+":"+request.getServerPort();
		    
		    actservice.sendVerificationEmail(userSave, appUrl); 
		    mod.addAttribute("inscrip", "Inscription réussie! Un mail de validation vous a été envoyé sur votre adresse mail.");
		    return signin(mod);	
			
		}	 
		
	}	
	
	// Process confirmation link

		@RequestMapping(value="/confirm", method = RequestMethod.GET)
		public String confirmRegistration(ModelMap mod, @RequestParam("token") String token,HttpServletRequest request) throws ParseException {
				
			Users user = actservice.findByConfirmationToken(token);
			System.out.println(user+"++");
				
			if (user == null) { // No token found in DB
				mod.addAttribute("invalidToken", "Votre compte a déjà été validé");
				return signin(mod);
			} 
			else 
			{ 				
				// Token found
				Boolean verified = actservice.verify(token);
				if(verified)
				{
					user.setConfirmationToken(null);
					user.setActive(true);
					Users newUser=actservice.saveUser(user);
					UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(newUser.getEmail());
					
			        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
			
			        SecurityContextHolder.getContext().setAuthentication(authentication);
			
			        request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());		
			        
					
					/*;
					mod.addAttribute("user", newUser);*/
			        
			        if(user.getPosteName()!=null)
			        {
			        	Employe emp=new Employe(user.getTelephone(), user.getPosteName(), user.getNom(), user.getPrenom());
			        	emp.setEmail(" ");
			        	empService.saveEmploye(emp);
			        	return "index1";
			        }
			        else
			        {
			        	Candidat candidat =new Candidat(user.getTelephone(),  user.getNom(), user.getPrenom());
						candidat.setEmail(" ");
						candidat=candidatService.saveCandidat(candidat);
						return "index1";
			        }
			        	
					
				}
				else
				{
					mod.addAttribute("valdate", "La durée de vérification de votre inscription est expirée , veuillez vous inscrire à nouveau");
					actservice.deleteUser(user);
					 if(user.getPosteName()!=null)
					    return addEmp(mod);
					 else
						return  signin(mod);
				}
												 
			}
									
		}
	
		
		

		

	
}

package com.app.gestionscolaire.webController;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.app.gestionscolaire.entity.Candidat;
import com.app.gestionscolaire.entity.Employe;
import com.app.gestionscolaire.entity.Role;
import com.app.gestionscolaire.entity.Users;
import com.app.gestionscolaire.repository.RoleRepository;
import com.app.gestionscolaire.service.AccountService;
import com.app.gestionscolaire.service.CandidatService;
import com.app.gestionscolaire.service.EmployeService;
import com.app.gestionscolaire.service.FileStorageService;
import com.app.gestionscolaire.service.UserDetailsServiceImpl;

@Controller
public class EmployeControlleur 

{
	@Autowired
	AccountService userService;
	@Autowired EmployeService empService;
	
	@Autowired
	private RoleRepository roleRepo;
	
	@RequestMapping("/employes")
	public String listemployes(ModelMap model)
	{
		List<Employe> employes=empService.findEmployes();
		List<Users> users=new ArrayList<>();
    	for(Employe e:employes)
    	{
    		Long idUser= (e.getIduser()-1);
    		users.add(userService.getUserById(idUser));
    	}
		model.addAttribute("users", users);
		model.addAttribute("employes",employes );
		return "employes";
	}
	
	

		

}

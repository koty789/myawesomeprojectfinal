package com.app.gestionscolaire.webController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.app.gestionscolaire.entity.Annee;
import com.app.gestionscolaire.entity.Matiere;
import com.app.gestionscolaire.service.AnneeService;

@Controller
public class AnneeControllerWeb {

	@Autowired
	private AnneeService service;
	
	
	@RequestMapping("/annee/list")
	public String ListAnnee(Model model) {
		List<Annee> annees = service.GetAllAnnee();
		model.addAttribute("annees", annees);	
		return "listAnnee";
	}
	
	@RequestMapping("/annee/new")
	public String NewAnnee(Model model) {
		Annee an = new Annee();
		model.addAttribute("annee", an);
		return "newannee";
	}
	
	
	@RequestMapping("annee/save")
	public String SaveAnnee(Model model, @ModelAttribute("annee") Annee annee) {
		service.SaveAnne(annee);
		return "redirect:/annee/list";
	}
	
	@RequestMapping(value="annee/delete", method = RequestMethod.POST)
	public String DeleteAnnee(Model model, @PathVariable("id") int id) {
		service.DeleteAnnee(id);
		return "redirect:/annee/AllAnnee";
	}
	
	@PostMapping("annee/edit/{id}")
	public String EditAnnee(Model model, @PathVariable("id") int id) {
		Annee annee= service.GetOneAnne(id);
		model.addAttribute("annee", annee);
		return "redirect:/annee/editAnnee";
	}
	
	@RequestMapping("annee/edition")
	public String modifAnnee(@ModelAttribute("annee") Annee annne) {
		
		 return "redirect:/annne/AllAnnee";
	}
	
}

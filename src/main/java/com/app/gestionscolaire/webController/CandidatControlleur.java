package com.app.gestionscolaire.webController;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.gestionscolaire.entity.Candidat;
import com.app.gestionscolaire.entity.Users;
import com.app.gestionscolaire.service.AccountService;
import com.app.gestionscolaire.service.CandidatService;



@Controller
public class CandidatControlleur 

{
	
	@Autowired
	CandidatService candidatService;
	@Autowired
	AccountService userService;
	
    @RequestMapping("/candidats")
    public String getCandidats(ModelMap model)
    {
    	List<Candidat> candidats=candidatService.getCandidats();
    	List<Users> users=new ArrayList<>();
    	for(Candidat c:candidats)
    	{
    		Long idUser= (c.getIduser()-1);
    		users.add(userService.getUserById(idUser));
    	}
    	model.addAttribute("candidats", candidats);
    	model.addAttribute("users", users);
    	
    	return "candidats";
    }
    
}

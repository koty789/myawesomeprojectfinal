package com.app.gestionscolaire.webController;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.app.gestionscolaire.entity.Admission;
import com.app.gestionscolaire.entity.Candidat;
import com.app.gestionscolaire.entity.Document;
import com.app.gestionscolaire.entity.Inscription;
import com.app.gestionscolaire.entity.UploadFileResponse;
import com.app.gestionscolaire.entity.Users;
import com.app.gestionscolaire.repository.RoleRepository;
import com.app.gestionscolaire.service.AccountService;
import com.app.gestionscolaire.service.AdmissionService;
import com.app.gestionscolaire.service.CandidatService;
import com.app.gestionscolaire.service.DocumentService;
import com.app.gestionscolaire.service.FileStorageService;
import com.app.gestionscolaire.service.NotificationService;

@Controller
public class AdmissionControllerWeb 
{
	@Autowired
	DocumentService docService;
	@Autowired
	AdmissionService admissionService;
	private org.springframework.security.core.Authentication authentication;
	@Autowired
	private AccountService actservice;
	@Autowired
	private CandidatService candidatService;
	@Autowired
	private RoleRepository roleRepo;
	@Autowired
	private FileStorageService fileStorageService;
	
	@Autowired
	private AdmissionService service;

	@RequestMapping("/demandead")
	public String demandeAdmission(ModelMap model) 
	{
		return "demandeAd";
	}
	
	@RequestMapping("/saveFile")
	public String safeFile(@RequestParam("files") MultipartFile[] files, ModelMap modelMap,RedirectAttributes redirectAttributes)throws IllegalStateException, IOException 
	{
		
		authentication =  SecurityContextHolder.getContext().getAuthentication();
    	Users currentUser= actservice.findByEmail(authentication.getName()) ;
    	Candidat candidat=candidatService.findCandidatById(currentUser.getIduser()+1);
    	Set<Document> documents=new HashSet<Document>();
    	LocalDate today = LocalDate.now();
		MultipartFile file=null;
		String folder ="./src/main/resources/static/files";
		///Créer une nouvelle admission
        Admission admission=new Admission();
        admission.setDate(today.toString());
        admission.setCandidat(candidat);
        admission=admissionService.SaveOneAdmission(admission);
		
		String message = "";
	   
	    	for (int i = 0; i < files.length; i++) 
	    	{
				file = files[i];
				
				Path path = Paths.get(folder + candidat.getIduser()+ file.getOriginalFilename());
		    
		      ///Créer un nouveau document
		        Document doc=new Document();
		        doc.setName(file.getOriginalFilename());
		        doc.setObservation(0);
			    doc.setDate(today.toString());			    
			    doc.setCandidat(candidat);
			    doc.setAdmission(admission);
			    documents.add(docService.SaveDocument(doc));	        
		      
	    	}           
	   
	    
		return "succesUpload";
	}
	
	@RequestMapping("/consulterAdmi/{id}")
	public String consulter(@PathVariable("id") Long id,ModelMap model)
	{
		Candidat candidat=candidatService.findCandidatById(id);
		Set<Admission> admissions=candidat.getAdmissions();
		model.addAttribute("admissions", admissions);
		return "candidatAdmis";
	}
	
	@RequestMapping("consulterAdmiDoc/{id}")
	public String consulterAdmiDoc(@PathVariable("id") Long id,ModelMap model)
	{
		Set<Document> documents=service.GetAdmissionById(id).getDocuments();
		model.addAttribute("documents", documents);
		
		return "listDocs";
	}
	
	@RequestMapping("/consulterDoc/{id}")
	public String consulterDoc(@PathVariable("id") Long id,ModelMap mod)
	{
		Document doc=docService.GetDocumentById(id);
		String name=doc.getCandidat().getIduser()+doc.getName();
		
		mod.addAttribute("name", name);
		return "viewDoc";
	}
	
	
	
	
	@RequestMapping("/admission/list")
	public String ListDocument(Model model) {
		List<Admission> admissions = service.GetAllAdmission();
		model.addAttribute("admissions", admissions);
		return "listAdmission";
	}
		
	@RequestMapping("/admission/new")
	public String NewAdmission(Model model) {
		Admission ad = new Admission();
		model.addAttribute("admission", ad);
		//model.addAttribute("notificationlist", notifService.GetAllNotification());
		return "newAdmission";
	}
		
	@RequestMapping("admission/save")
	public String SaveInscrp(Model model, @ModelAttribute("admission") Admission admission) {
		service.SaveOneAdmission(admission);
		return "redirect:/admission/list";
	}
	
	@GetMapping("admission/edit/{id}")
	public String EditerAdmission(Model model, @PathVariable("id") Long id) {
		Admission admission = service.GetAdmissionById(id);
		model.addAttribute("admission", admission);
		return "redirect:/admission/modifAdmission";
	}
	
	
	@PostMapping("admission/edition")
	public String EditAdmission(@ModelAttribute("admission") Admission admission) {
		return "redirect:/admission/editAdmission";
	}
	
	@RequestMapping("admission/delete")
	public String SupprimerAdmission(Model model, @ModelAttribute("id") Long id) {
		service.DeleteAdmission(id);
		return "redirect:/admission/ListAdmission";
	}
	
	
	
	
	

}

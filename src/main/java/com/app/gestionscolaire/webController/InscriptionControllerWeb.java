package com.app.gestionscolaire.webController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.app.gestionscolaire.entity.Annee;
import com.app.gestionscolaire.entity.Inscription;
import com.app.gestionscolaire.service.InscriptionService;

@Controller
public class InscriptionControllerWeb {
	
	@Autowired
	private InscriptionService service;

	@RequestMapping("/inscription/list")
	public String ListDocument(Model model) {
		List<Inscription> inscriptions = service.getAllInscription();
		model.addAttribute("inscriptions", inscriptions);
		return "listInscript";
	}
	
	
	@RequestMapping("/inscription/new")
	public String NewInscription(Model model) {
		Inscription an = new Inscription();
		model.addAttribute("inscription", an);
		return "newInscription";
	}
	
	
	@RequestMapping("inscription/save")
	public String SaveInscrp(Model model, @ModelAttribute("inscription") Inscription inscription) {
		service.SaveOneInscription(inscription);
		return "redirect:/inscription/list";
	}
	
	@GetMapping("inscription/edit/{id}")
	public String EditerInscription(Model model, @PathVariable("id") Long id) {
		Inscription inscription = service.GetInscriptionById(id);
		model.addAttribute("inscription", inscription);
		return "redirect:/inscription/modifInscription";
	}
	
	
	@PostMapping("inscription/edition")
	public String EditInscription(@ModelAttribute("inscription") Inscription inscription) {
		return "redirect:/inscription/editInscription";
	}
	
	@RequestMapping("inscription/delete")
	public String SupprimerInscription(Model model, @ModelAttribute("id") Long id) {
		service.DeleteInscription(id);
		return "redirect:/inscription/ListInscription";
	}
}

package com.app.gestionscolaire.webController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.gestionscolaire.entity.Suivis;
import com.app.gestionscolaire.service.SuivisService;

@Controller
public class SuivisControllerWeb {
	
	@Autowired
	private SuivisService service;

	@RequestMapping("/suivis/list")
	public String ListDocument(Model model) {
		List<Suivis> suivis = service.GetAllSuivis();
		model.addAttribute("suivis", suivis);
		return "listSuiv";
	}
	
	
	@RequestMapping("/suivis/new")
	public String NewSuivis(Model model) {
		Suivis suiv = new Suivis();
		model.addAttribute("suivis",suiv);
		return "newSuiv";
	}
	
	
	@RequestMapping("suivis/save")
	public String SaveInscrp(Model model, @ModelAttribute("suivis") Suivis suivis) {
		service.SaveOneSuivis(suivis);
		return "redirect:/suivis/list";
	}
	
	@GetMapping("suivis/edit/{id}")
	public String EditerSuivis(Model model, @PathVariable("id") Long id) {
		Suivis suivis = service.GetSuivisById(id);
		model.addAttribute("suivis", suivis);
		return "redirect:/Suivis/modifSuivis";
	}
	
	
	@PostMapping("suivis/edition")
	public String EditSuivis(@ModelAttribute("suivis") Suivis suivis) {
		return "redirect:/suivis/editSuivis";
	}
	
	@RequestMapping("Suivis/delete")
	public String SupprimerSuivis(Model model, @ModelAttribute("id") Long id) {
		service.DeleteSuivis(id);
		return "redirect:/Suivis/ListSuivis";
	}

}

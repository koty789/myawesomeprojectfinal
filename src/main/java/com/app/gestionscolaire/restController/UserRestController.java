package com.app.gestionscolaire.restController;
import java.util.List;

import com.app.gestionscolaire.entity.Users;
import com.app.gestionscolaire.repository.UserRepository;
import com.app.gestionscolaire.service.AccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController  //MVC coté client
public class UserRestController 
{
	
	@Autowired
	private UserRepository userRepo;
	
		
		@RequestMapping("/users")
		public List<Users> getUsers()
		{
			return userRepo.findAll();
		}
		
}

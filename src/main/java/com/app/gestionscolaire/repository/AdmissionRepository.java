package com.app.gestionscolaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.gestionscolaire.entity.Admission;

public interface AdmissionRepository  extends JpaRepository<Admission, Long>{

}

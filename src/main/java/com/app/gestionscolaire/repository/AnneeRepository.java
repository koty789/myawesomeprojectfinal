package com.app.gestionscolaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.gestionscolaire.entity.Annee;

public interface AnneeRepository  extends JpaRepository<Annee, Integer>{

}

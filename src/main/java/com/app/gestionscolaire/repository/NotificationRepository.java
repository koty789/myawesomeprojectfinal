package com.app.gestionscolaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.gestionscolaire.entity.Notification;

public interface NotificationRepository  extends JpaRepository<Notification, Long>{

}

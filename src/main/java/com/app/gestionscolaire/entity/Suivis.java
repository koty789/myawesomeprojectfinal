package com.app.gestionscolaire.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity(name="suivis")
public class Suivis {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSuivis;
	private String operation;
	private String date;
	
	@ManyToOne
    @JoinColumn(name="suivis_id", nullable=false)
	private Inscription inscriptionSuiv;
	
	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idAdmission")
	private Admission admission;
	
	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idEmploye")
	private Employe employe;
	
	@ManyToOne
    @JoinColumn(name="notification_id", nullable=false)
	private Notification notification;

	public Long getIdSuivis() {
		return idSuivis;
	}

	public void setIdSuivis(Long idSuivis) {
		this.idSuivis = idSuivis;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Inscription getInscriptionSuiv() {
		return inscriptionSuiv;
	}

	public void setInscriptionSuiv(Inscription inscriptionSuiv) {
		this.inscriptionSuiv = inscriptionSuiv;
	}

	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
}

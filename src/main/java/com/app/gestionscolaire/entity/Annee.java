package com.app.gestionscolaire.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="anne")
public class Annee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idAnne")
	private int idAnne;
	
	@Column(name="niveau")
	private int niveau;
	
	public Annee() {
		
	}

	public Annee(int idAnne, int niveau) {
		super();
		this.idAnne = idAnne;
		this.niveau = niveau;
	}

	public int getIdAnne() {
		return idAnne;
	}

	public void setIdAnne(int idAnne) {
		this.idAnne = idAnne;
	}

	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	
	
}

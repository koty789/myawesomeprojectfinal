package com.app.gestionscolaire.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;


@Entity
public class Candidat extends Users 
{

	private String nationalite;

	@OneToMany(mappedBy="candidat")
	private Set<Admission> admissions =new HashSet<Admission>();
	
	@OneToMany(mappedBy="candidat")
	private Set<Document> documents =new HashSet<Document>();
	

    public Candidat() 
    {
    	
    }
    
	
	public String getNationalite() {
		return nationalite;
	}
	
	
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public Candidat(String telephone, String nom, String prenom) {
		super(telephone, nom, prenom);
	}


	public Set<Admission> getAdmissions() {
		return admissions;
	}


	public void setAdmissions(Set<Admission> admissions) {
		this.admissions = admissions;
	}


	public Set<Document> getDocuments() {
		return documents;
	}


	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}

    
    

    

    




}
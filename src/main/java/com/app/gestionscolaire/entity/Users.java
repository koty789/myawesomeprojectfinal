package com.app.gestionscolaire.entity;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class Users  {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long iduser;
	private String password;	
	private Boolean active;
	private String telephone;
	private String posteName;
	
	
	@NotEmpty
	@Column(unique=true,nullable = true)
	@Email
	private String email;
	@Column(nullable = false,length = 20)
	private String nom;
	@Column(nullable = false,length = 20)
	private String prenom;	
	private String photo;
	
	@ManyToMany(fetch = FetchType.EAGER)
	Set<Role> roles=new HashSet<>();
	private String confirmationToken;
	public Long getIduser() {
		return iduser;
	}
	public void setIduser(Long iduser) {
		this.iduser = iduser;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	public Users() {
		super();
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	
	public Users(Long iduser,  Boolean active, 
			String nom, String prenom) {
		super();
		this.iduser = iduser;
		
		
		this.active = active;
		
		this.nom = nom;
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getConfirmationToken() {
		return confirmationToken;
	}
	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getPosteName() {
		return posteName;
	}
	public void setPosteName(String posteName) {
		this.posteName = posteName;
	}
	public Users(String telephone, String posteName, String nom, String prenom) {
		super();
		this.telephone = telephone;
		this.posteName = posteName;
		this.nom = nom;
		this.prenom = prenom;
	}
	public Users(String telephone, String nom, String prenom) {
		super();
		this.telephone = telephone;
		this.nom = nom;
		this.prenom = prenom;
	}
	
	
}

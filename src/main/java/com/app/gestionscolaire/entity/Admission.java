package com.app.gestionscolaire.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity()
public class Admission {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	private Long idAdmission;	
	private String operation;
	private int etat;
	private String date;

	@OneToMany(mappedBy="admission")
	private Set<Inscription> inscriptions =new HashSet<Inscription>();
	@OneToMany(mappedBy="admission")
	private Set<Document> documents =new HashSet<Document>();
	
	@ManyToOne
    @JoinColumn(name="candidat_id", nullable=false)
	private Candidat candidat;
	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
	@JoinColumn(name = "idEmploye"/*, nullable = false*/)
	private Employe employe;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "admission",cascade = CascadeType.PERSIST)
	private Set<Suivis> suivis = new HashSet<Suivis>(0);
	

	public Admission() {
		super();
		
	}

	public Admission(Long idAdmission, String operation, int etat) {
		super();
		this.idAdmission = idAdmission;
		this.operation = operation;
		this.etat = etat;
	
	}

	public Long getIdAdmission() {
		return idAdmission;
	}

	public void setIdAdmission(Long idAdmission) {
		this.idAdmission = idAdmission;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Set<Inscription> getInscriptions() {
		return inscriptions;
	}

	public void setInscriptions(Set<Inscription> inscriptions) {
		this.inscriptions = inscriptions;
	}

	public Set<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}

	public Candidat getCandidat() {
		return candidat;
	}

	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	public Set<Suivis> getSuivis() {
		return suivis;
	}

	public void setSuivis(Set<Suivis> suivis) {
		this.suivis = suivis;
	}

	
	
	
}

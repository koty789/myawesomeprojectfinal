package com.app.gestionscolaire.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity(name="notification")
public class Notification {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idNotification")
	private Long idNotification;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="date")
	private Date date;
	
	@Column(name="contenu")
	private String contenu;
	
	@Column(name="adresseDepart")
	private String adresseDepart;
	
	@Column(name="adressArrive")
	private String adresseArrive;

	@OneToMany(mappedBy="notification")
	private Set<Suivis> suivis = new HashSet<Suivis>();
	
	
	
	public Notification() {
	}

	public Notification(Long idNotification, Date date, String contenu, String adresseDepart, String adresseArrive) {
		super();
		this.idNotification = idNotification;
		this.date = date;
		this.contenu = contenu;
		this.adresseDepart = adresseDepart;
		this.adresseArrive = adresseArrive;
	}

	public Long getIdNotification() {
		return idNotification;
	}

	public void setIdNotification(Long idNotification) {
		this.idNotification = idNotification;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public String getAdresseDepart() {
		return adresseDepart;
	}

	public void setAdresseDepart(String adresseDepart) {
		this.adresseDepart = adresseDepart;
	}

	public String getAdresseArrive() {
		return adresseArrive;
	}

	public void setAdresseArrive(String adresseArrive) {
		this.adresseArrive = adresseArrive;
	}
	
	
	

}

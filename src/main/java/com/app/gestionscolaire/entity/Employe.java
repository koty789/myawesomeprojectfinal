package com.app.gestionscolaire.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;



@Entity
public class Employe extends Users
{
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employe",cascade = CascadeType.PERSIST)
	private Set<Suivis> suivis = new HashSet<Suivis>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employe",cascade = CascadeType.PERSIST)
	private Set<Admission> admissions = new HashSet<Admission>(0);
	
	@OneToMany(mappedBy="candidat")
	private Set<Document> documents =new HashSet<Document>();
	
	public Employe() 
	{
		super();
		
	}

	public Employe(String telephone, String posteName, String nom, String prenom) {
		super(telephone, posteName, nom, prenom);
		
	}

	
	
	
	

	
	
	
	
   
}
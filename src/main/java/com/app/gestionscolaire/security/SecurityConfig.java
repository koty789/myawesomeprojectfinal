package com.app.gestionscolaire.security;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import com.app.gestionscolaire.service.UserDetailsServiceImpl;




@Configuration
@EnableWebSecurity

public class SecurityConfig extends WebSecurityConfigurerAdapter

{
		@Autowired
		private UserDetailsServiceImpl userDetailsServiceImpl;
		
		       
	    @Bean
	    PasswordEncoder passwordEncoder() {
	        return new BCryptPasswordEncoder();
	    }
	     
	    @Bean
	    public DaoAuthenticationProvider authenticationProvider() {
	        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
	        authProvider.setUserDetailsService(this.userDetailsServiceImpl);
	        authProvider.setPasswordEncoder(passwordEncoder());
	         
	        return authProvider;
	    }
	 
	    @Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	        auth.authenticationProvider(authenticationProvider());
	    }
	 
	
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception 
	{
		/*//.usernameParameter("email")
		//http.httpBasic(); usernameParameter("email") //Pour un formulaire d'authentification basic
		http.formLogin().loginPage("/login");
		http.formLogin()
	    .defaultSuccessUrl("/candidats");
		http.authorizeRequests().antMatchers("/admin/**").hasRole("ADMIN");
		http.authorizeRequests().antMatchers("/","/**","/login","/webjars/**").permitAll();//Permet d'autoriser les ressources et certaines pages sans auth
		
		//http.authorizeRequests().anyRequest().authenticated();//Toutes les réquetes sont bloqués à condt de s'auth,en la commentant certaines pages non protégées seront accessibles 
		//Il vaut mieux la decommenter et liberer la page que vous souhaitez ouvrir à tout le monde
		
		http.exceptionHandling().accessDeniedPage("/notAutorized");//Page d'erreur */
		
		

		http.authorizeRequests()
				.antMatchers("/**").permitAll()
				
				
				//.antMatchers("/candidats").hasAuthority("ADMIN")
				.anyRequest().authenticated()
				.and()
				.formLogin()
				.usernameParameter("email")
				.loginPage("/login").permitAll()
				.defaultSuccessUrl("/acceuil")
				;
		
	    
	}
	//Ce objet pourra être injecter dans les autres parties de l'appli en ajoutant le bean
	
	
	@Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**", "/static/**", "/vendor/**", "/css/**",  "/js/**", "/images/**","/fonts/**","/img/**","/scss/**", "/docs/**", "/assets/**");
    }
	
	
	
}
